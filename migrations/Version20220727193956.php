<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220727193956 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book_rending (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, renting_start DATE NOT NULL, renting_end DATE NOT NULL, limit_date DATE NOT NULL, INDEX IDX_D06A4306A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_rending ADD CONSTRAINT FK_D06A4306A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE book ADD book_rending_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A33114FF2DD2 FOREIGN KEY (book_rending_id) REFERENCES book_rending (id)');
        $this->addSql('CREATE INDEX IDX_CBE5A33114FF2DD2 ON book (book_rending_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book DROP FOREIGN KEY FK_CBE5A33114FF2DD2');
        $this->addSql('DROP TABLE book_rending');
        $this->addSql('DROP INDEX IDX_CBE5A33114FF2DD2 ON book');
        $this->addSql('ALTER TABLE book DROP book_rending_id');
    }
}
