<?php

namespace App\Entity;

use Date;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BookRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity=BookCategory::class, inversedBy="book")
     */
    private $bookCategory;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity=BookRending::class, inversedBy="book")
     */
    private $bookRending;
    public function __construct()
    {
        $this->booCategory = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getBookCategory(): ?BookCategory
    {
        return $this->bookCategory;
    }

    public function setBookCategory(?BookCategory $bookCategory): self
    {
        $this->bookCategory = $bookCategory;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getBookRending(): ?BookRending
    {
        return $this->bookRending;
    }

    public function setBookRending(?BookRending $bookRending): self
    {
        $this->bookRending = $bookRending;

        return $this;
    }
}
