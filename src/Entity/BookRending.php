<?php

namespace App\Entity;

use App\Repository\BookRendingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRendingRepository::class)
 */
class BookRending
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bookRendings")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="bookRending")
     */
    private $book;

    /**
     * @ORM\Column(type="date")
     */
    private $rentingStart;

    /**
     * @ORM\Column(type="date")
     */
    private $rentingEnd;

    /**
     * @ORM\Column(type="date")
     */
    private $limitDate;

    public function __construct()
    {
        $this->book = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Book>
     */
    public function getBook(): Collection
    {
        return $this->book;
    }

    public function addBook(Book $book): self
    {
        if (!$this->book->contains($book)) {
            $this->book[] = $book;
            $book->setBookRending($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->book->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getBookRending() === $this) {
                $book->setBookRending(null);
            }
        }

        return $this;
    }

    public function getRentingStart(): ?\DateTimeInterface
    {
        return $this->rentingStart;
    }

    public function setRentingStart(\DateTimeInterface $rentingStart): self
    {
        $this->rentingStart = $rentingStart;

        return $this;
    }

    public function getRentingEnd(): ?\DateTimeInterface
    {
        return $this->rentingEnd;
    }

    public function setRentingEnd(\DateTimeInterface $rentingEnd): self
    {
        $this->rentingEnd = $rentingEnd;

        return $this;
    }

    public function getLimitDate(): ?\DateTimeInterface
    {
        return $this->limitDate;
    }

    public function setLimitDate(\DateTimeInterface $limitDate): self
    {
        $this->limitDate = $limitDate;

        return $this;
    }
}
