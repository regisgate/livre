<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\BookCategory;
use App\Entity\BookRending;
use Faker;
use App\Entity\User;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create();

        $users = [];
        for($i=0; $i<50; $i++){

            $user = new User();
            $user->setLastname( $faker->lastName());
            $user->setFirstname( $faker->firstName());
            $user->setCreatedAt(new \DateTime());
            $user->setPhone( $faker->phone()); 
            $user->setEmail( $faker->email);
            $user->setPassword($faker->password());
            $manager->persist($user);
            $users[] = $user;
        }
        $categories = [];
        for($i = 0; $i<15; $i++){
            $category = new Category();
            $category->setLabel($faker->text(50));
            $manager->persist($category);
            $categories[] = $category;

        }
    
        for($i=0; $i<60; $i++){
            $book = new Book();
            $book->setReference($faker->text(50));
            $book->setAuthor($users[$faker->numberBetween(0, 49)]);
            $manager->persist($book);
           
        }

        for($i=0; $i<60; $i++){
            $bookCategory = new BookCategory();
            $bookCategory->setCategory($categories[$faker->numberBetween(0, 20)]);
            $bookCategory->addBook($users[$faker->numberBetween(0, 50)]);
            $manager->persist($book);
        }
       $authors = [];
        for($i=0; $i<50; $i++){

            $author = new Author();
            $author->setLastname( $faker->lastName());
            $author->setFirstname( $faker->firstName());
            $manager->persist($author);
            $authors[] = $author;
        } 
        
        $bookRendings = [];
        for($i=0; $i<50; $i++){

            $bookRending = new BookRending();
            $bookRending->setUser($bookRendings[$faker->numberBetween(0, 50)]);
            $bookRending->addBook($bookRendings[$faker->numberBetween(0, 20)]);
            $bookRending->setRentingStart(new \DateTime());
            $bookRending->setRentingEnd(new \DateTime());
            $bookRending->setLimitDate(new \DateTime());
            $manager->persist($bookRending);
            $bookRendings[] = $bookRending;
        }

        $manager->flush();
    }
}
