<?php

namespace App\Controller\Admin;

use App\Entity\BookCategory;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BookCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BookCategory::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
