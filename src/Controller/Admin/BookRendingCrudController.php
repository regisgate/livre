<?php

namespace App\Controller\Admin;

use App\Entity\BookRending;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BookRendingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BookRending::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
