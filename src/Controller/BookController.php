<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\BookRending;
use App\Entity\Category;
use App\Entity\User;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookController extends AbstractController
{

    private $repoBook;
    private $repoCategory;

    public function __construct(BookRepository $repoBook, CategoryRepository $repoCategory)
    {
        $this->repoBook = $repoBook;
        $this->repoCategory;
    }
    /**
     * @Route("/book", name="book")
     */
    public function index(): Response
    {
        $categories = $this->repoCategory->findAll();
        $books = $this->repoBook->findAll();
        return $this->render('book/index.html.twig', [
            'books' => $books,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/book/{id}", name="show")
     */
    public function show(Book $book): Response
    {
        if(!$book){
            return $this->redirectToRoute('book');
        }
        return $this->render('book/show.html.twig', [
            'book' => $book
          
        ]);
    }
    /**
     * @Route("/showBook/{id}", name="show_book")
     */
    public function showCategory(?Category $category): Response
    {
       if($category){
        $books = $category->getBooks->getValues();
       }else{
        $books = null;
        return $this->redirectToRoute('book');
       }
       $categories = $this->repoCategory->findAll();

        return $this->render('book/index.html.twig', [
            'books' => $books,
            'categories' => $categories
        ]);
    }
}
